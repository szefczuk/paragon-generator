const puppeteer = require('puppeteer');
const path = require('path');

(async () => {
    const browser = await puppeteer.launch({headless: true});
    const page = await browser.newPage();
    await page.goto("file://" + path.resolve(__dirname, './index.html'));
    const pageHeight = await page.evaluate(() => document.documentElement.querySelector('body').offsetHeight);
    const pageWidth = await page.evaluate(() => document.documentElement.querySelector('body').clientWidth);
    await page.setViewport({
        width: pageWidth,
        height: pageHeight
    });
    await page.screenshot({
        path: `png/${new Date().toISOString()}.png`,
        clip: {
            x: 0,
            y: 0,
            width: pageWidth,
            height: pageHeight,
        },
    });
    await browser.close();
})();

